const {By,until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const request = require("request");
const{assert}=require('chai');

module.exports= function PageObj() {
    const pageObjects = {

        getImages:By.xpath('//div[@class=\"example\"]//img'),
        slider:By.css('.sliderContainer>input')


    };
    return {



        verifyImages:async function(driver){
                this.resolveAfter2Seconds();
                await driver.findElements(pageObjects.getImages).then(async function(count) {
                    for(let i=1;i<=count.length;i++){
                        const getImage=await driver.findElement(By.xpath('(//div[@class="example"]//img)['+i+']')).getAttribute("src");
                        request.get(getImage,function (error, response, body) {

                            let status=response.statusCode;


                            if(status===200){
                                assert.equal(status,200);
                                console.log(status);
                            }
                            else{
                                assert.equal(status,404);
                                console.log(status);
                            }

                        });
                    }

                })

        },
        moveSliderToRight:async function(driver){
          /* await driver.findElement(By.css('.sliderContainer>input')).sendKeys("5").then(async function () {
              const getValue=await driver.findElement(By.css('span#range')).getText();
              assert.equal(getValue,"5")*/
            const actions=driver.actions({async :true})
               const kb=actions.keyboard();
              const mouse=actions.mouseMove;
              await actions.click('.sliderContainer>input').sendKeys("5").perform();

          // })


        },
        moveSliderToLeft:async function(driver){
            await driver.findElement(By.css('.sliderContainer>input')).sendKeys("0").then(async function () {
                const getValue=await driver.findElement(By.css('span#range')).getText();
                assert.equal(getValue,"5")
            })


        },

        resolveAfter2Seconds: function () {
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve('resolved');
                }, 4000);
                console.log('waited for 2000ms');
            });
        },
    };
}