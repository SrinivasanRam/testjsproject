'use strict';
let {assert} = require('chai');
let webdriver = require('selenium-webdriver');
let chromeCapabilities = webdriver.Capabilities.chrome();
require('chromedriver');
let mocha = require('mocha');
let describe = mocha.describe;
let it = mocha.it;
const fs=require('fs');
let runOrder=1;

const commonFns=require('./pageObjectModule/testFunctions.js');
let contents = fs.readFileSync('testJSApp/src/test/retrieveData/dateFile.json');
let jsonContent = JSON.parse(contents);
let imageUrl=jsonContent.brokenImagesURL;
let basicAuth=jsonContent.basicAuthURL;
let moveSliderURL=jsonContent.moveSliderURL;
let hoverURL=jsonContent.hoverURL;
chromeCapabilities.set('chromeOptions', {'args': [ '--ignore-certificate-errors-pki-list','--ignore-ssl-errors','--no-sandbox', 'window-size=1200,1000' , '--disable-gpu'] });
const driver=new webdriver.Builder().forBrowser("chrome").withCapabilities(chromeCapabilities).build();

describe('Test_1',function () {

    let commonPages=new commonFns();
    this.timeout(15000);



    async function beforeMethod(urls){
        await driver.get(urls).then(async function () {
            await driver.manage().window().maximize(1200, 800);


        });

    }


    xit('verify the images broken',function(done){
        assertRunOrder(1)
        beforeMethod(imageUrl).then(function () {
            commonPages.verifyImages(driver);
        });



        setTimeout(done,300);
    })
    it('verify the moveSlider actions',function (done) {
        //assertRunOrder(2)
        beforeMethod(moveSliderURL).then(async function () {
            await commonPages.moveSliderToRight(driver);

        }).then(async function () {
         //  await commonPages.moveSliderToLeft(driver);
        })


        setTimeout(done,300);
    })
});
function assertRunOrder(expectedRunOrder) {
    assert.equal(runOrder++, expectedRunOrder);
}